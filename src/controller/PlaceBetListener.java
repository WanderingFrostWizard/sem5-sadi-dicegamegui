package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.GUIPlaceBetBox;
import view.GUIWindow;

public class PlaceBetListener implements ActionListener
{
	private GUIWindow gui;
	private GUIPlaceBetBox betBox;
	
	public PlaceBetListener( GUIWindow gui, GUIPlaceBetBox betBox )
	{
		this.gui = gui;
		this.betBox = betBox;
	}
	
	@Override
	public void actionPerformed(ActionEvent event )
	{
		String betStr = betBox.getBetTxt();
		int betInt;
		
		// IF an invalid player is selected DONT PLACE BET
		//TODO  IMPROVE THIS ###################################################
		if ( gui.getSelectedPlayerID().equals("0") )
		{
			System.out.println("Invalid player selected");
			return;
		}
		if ( gui.DEBUG )
			System.out.println("Selected Player - " + gui.getSelectedPlayerID() );
		
		try
		{
			betInt = Integer.parseInt( betStr );
		}
		catch ( NumberFormatException ex )
		{
			System.out.println("ERROR - INVALID characters detected in bet field");
			return;
		}
		
		gui.placeBet( betInt );
		
		// Disable the Bet button, to stop the player from placing multiple bets
		betBox.enableBetButton( false );
		
		// Clear the bet text box within GUIPlaceBetBox to avoid any confusion
		// during gameplay
		betBox.resetTxt();
	}
}
