package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.GUIAddPlayerBox;
import view.GUIWindow;

public class AddPlayerListener implements ActionListener
{
	// Reference to GUIWindow
	private GUIWindow gui;
	
	// Reference to parent class
	private GUIAddPlayerBox addPlayerBox;
	
	public AddPlayerListener( GUIWindow gui, GUIAddPlayerBox addPlayerBox )
	{
		this.gui = gui;
		this.addPlayerBox = addPlayerBox;
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		String pointsStr = addPlayerBox.getPointsFieldText();
		int pointsInt;
		
		System.out.println( "Name - " + addPlayerBox.getNameFieldText() + "  Points - " + pointsStr );
		
		// Check the entered string is an INTEGER
		try
		{
			pointsInt = Integer.parseInt( pointsStr );
		}
		catch ( NumberFormatException ex )
		{
			System.out.println("ERROR - INVALID characters detected in Initial Points");
			return;
		}
		gui.createPlayer( addPlayerBox.getNameFieldText(), pointsInt );
		
		// Clear the text boxes within GUIAddPlayerBox to avoid any confusion
		// during gameplay
		addPlayerBox.resetTxt();
	}
}
