package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.GUIWindow;

public class EndRoundBtnListener implements ActionListener
{
	// Reference to GUIWindow
	private GUIWindow gui;
	
	public EndRoundBtnListener( GUIWindow gui )
	{
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		gui.clearLastGame();		
	}
}
