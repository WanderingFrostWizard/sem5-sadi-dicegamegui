package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.GUIWindow;

/**
 * Action Listener which monitors the player menu
 * This takes the player selected and returns the number back to the GUI
 */
public class PlayerSelectMenuListener implements ActionListener
{
	// Create reference to parent object
	private GUIWindow gui;
	
	public PlayerSelectMenuListener( GUIWindow gui )
	{
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		String playerID = event.getActionCommand();
		if ( gui.DEBUG )
			System.out.println( "Selected Player - " + playerID );	
		
		// NOTE selectedPlayer is a string, to match the original source code
		gui.selectCurrentPlayer( playerID );
	}
}
