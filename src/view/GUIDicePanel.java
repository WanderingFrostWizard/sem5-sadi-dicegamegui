package view;

import java.awt.GridLayout;
import javax.swing.JPanel;

import model.interfaces.DicePair;
import model.interfaces.Player;

@SuppressWarnings("serial")
public class GUIDicePanel extends JPanel
{
	private GUIDice dice1;
	private GUIDice dice2;
	private GUIResultsPanel results;

	public GUIDicePanel( )
	{
		this.setLayout( new GridLayout() );
		
		// Create both Dice
		dice1 = new GUIDice();
		dice2 = new GUIDice();
		
		//TODO Set borders on the dice
		
		// Create Results Panel
		results = new GUIResultsPanel( );
		
		add( dice1 );
		add( dice2 );
		add( results );
	}
	
	public void changePlayer( Player player )
	{
		results.updateName( player.getPlayerName() );
		
		// Check if there IS a player result else, it will be null
		int dice1Value = 0;
		int dice2Value = 0;
		
		DicePair result = player.getRollResult();
		
		if ( result != null )
		{
			dice1Value = result.getDice1();
			dice2Value = result.getDice2();
		}

		setResults( dice1Value, dice2Value );
	}
	
	/*
	 * This has been separated from changePlayer(...), as the "House" will 
	 * never change their name, ONLY their results
	 */
	public void setResults( int dice1Value, int dice2Value )
	{
		// Update the dice
		dice1.displayValue( dice1Value );
		dice2.displayValue( dice2Value );
		
		results.updateTotal( dice1Value + dice2Value );
	}
	
	public void setHouse()
	{
		results.updateName( "HOUSE" );
	}
}
