package view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This class is used to create a panel to be added beside the two dice, to 
 * display the results of the two dice
 * 
 * ------------
 * |  PLAYER  |      nameTxt
 * |  TOTALS  |      LOCAL variable
 * |    11    |      totalTxt
 * ------------
 * 
 */
@SuppressWarnings("serial")
public class GUIResultsPanel extends JPanel
{
	private JLabel nameTxt;
	private JLabel totalTxt;
	
	public GUIResultsPanel( )
	{
		Dimension textSize = new Dimension( 100, 30 );
		
		this.setLayout( new GridLayout() );
		
		nameTxt = new JLabel( );
		nameTxt.setMinimumSize( textSize );
		nameTxt.setHorizontalAlignment( JTextField.CENTER );
		
		JLabel infoTxt = new JLabel( "TOTALS" );
		infoTxt.setMinimumSize( textSize );
		infoTxt.setHorizontalAlignment( JTextField.CENTER );
		
		totalTxt = new JLabel();
		totalTxt.setMinimumSize( textSize );
		totalTxt.setHorizontalAlignment( JTextField.CENTER );
		
		add(nameTxt);
		add(infoTxt);
		add(totalTxt);
	}
	
	/**
	 * Set the label totalTxt to display the players / houses total dice value
	 * @param total  value to be set
	 */
	public void updateTotal( int total )
	{
		totalTxt.setText( String.valueOf( total ));
	}
	
	public void updateName( String playerName )
	{
		nameTxt.setText( playerName );
	}
	
}
