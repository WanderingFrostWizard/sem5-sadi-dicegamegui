package view;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import controller.PlaceBetListener;

@SuppressWarnings("serial")
public class GUIPlaceBetBox extends JPanel
{
	private JTextField betField;
	private JButton placeBetBtn;
	
	public String getBetTxt()
	{
		return betField.getText();
	}
	
	public GUIPlaceBetBox( GUIWindow gui )
	{
		this.setLayout( new GridLayout() );
		
		JLabel betTxt = new JLabel("Bet to Place");
		betTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		
		betField = new JTextField();
		betField.setHorizontalAlignment( JTextField.CENTER );
		betField.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		placeBetBtn = new JButton("Place Bet");
		placeBetBtn.setBorder(new BevelBorder(BevelBorder.RAISED));
		placeBetBtn.addActionListener( new PlaceBetListener( gui, this ) );
		
		// Add components to the JPanel
		add(betTxt);
		add(betField);
		add(placeBetBtn);
	}
	
	public void enableBetButton( boolean status )
	{
		placeBetBtn.setEnabled( status );
	}
	
	public void resetTxt()
	{
		betField.setText( "" );
	}
}
