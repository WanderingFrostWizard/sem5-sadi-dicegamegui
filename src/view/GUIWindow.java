package view;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.SimplePlayer;
import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.Player;

/**
 * This class is used to produce the GUI, it calls each of the components
 * and displays them as follows
 * 
 * --------------------------------------------
 * | GUIMenuBar                               |
 * --------------------------------------------
 * | GUIAddPlayerBox                          |
 * --------------------------------------------
 * | GUIPlaceBetBox                           |
 * --------------------------------------------
 * | GUIDicePanel - ( stored as playerPanel ) |
 * |      This panel has the following        |
 * |  GUIDice    GUIDice     GUIResultsPanel  |
 * --------------------------------------------
 * | GUIResultsBar                            |
 * --------------------------------------------
 * | GUIDicePanel - ( stored as housePanel )  |
 * |      This panel has the following        |
 * |  GUIDice    GUIDice     GUIResultsPanel  |
 * --------------------------------------------
 * | GUIStatusBar                             |
 * --------------------------------------------
 */
@SuppressWarnings("serial")
public class GUIWindow extends JPanel
{
	// Window Constants
	public static final int WIDTH = 800;
	public static final int HEIGHT = 800;
	
	// Roll timing Constants
	static public final int INITIALDELAY = 1;
	static public final int FINALDELAY = 100;
	static public final int DELAYINCREMENT = 20;
	
	public final boolean DEBUG = false;
	
	// GameEngine reference to allow access to methods
	private GameEngine gameEngine;
	
	// GUI specific variables
	private JFrame guiFrame;
	private GUIMenuBar guiMenuBar;
	private GUIPlaceBetBox placeBetBox;
	private GUIDicePanel playerPanel;
	private GUIResultsBar resultsBar;
	private GUIDicePanel housePanel;
	private GUIStatusBar statusBar;

	private final int GUIROWS = 6;
	
	
	// NOTE that player ID's are stored as STRINGS (from source code)
	private String selectedPlayer = "0";
	private Player currentPlayer;
	
	private int noOfPlayersLoaded = 0;
	
	// ------------------ GETTERS / SETTERS ------------------
	// This is used in the PlaceBetListener, which specifically needs the 
	// current PlayerID
	public String getSelectedPlayerID()
	{
		return selectedPlayer;
	}
	
	// -------------- END OF GETTERS / SETTERS ---------------
	
	/**
	 * Produces the JFrame, used to display the GUI window
	 */
	public GUIWindow( GameEngine gameEngine )
	{
		// Store reference to the gameEngine
		this.gameEngine = gameEngine;
		
	    //Create and set up the window.
		guiFrame = new JFrame("DiceGame"); 
			
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setSize(WIDTH, HEIGHT);
	
		guiFrame.setLayout(new GridLayout(GUIROWS, 0 ));
		
		//---------------------------------------------------------------------
	    //-------------------- Add content to the window. --------------------- 
		guiMenuBar = new GUIMenuBar( this );
		guiFrame.setJMenuBar( guiMenuBar.getMenuBarObj() );
		
		// PlayerBox
		guiFrame.add( new GUIAddPlayerBox( this ) );
		
		// PlaceBetBox
		placeBetBox = new GUIPlaceBetBox( this );
		guiFrame.add( placeBetBox );
	
		// playerPanel
		playerPanel = new GUIDicePanel( );
		guiFrame.add( playerPanel );
		
		// resultsBar
		resultsBar = new GUIResultsBar( this );
		guiFrame.add( resultsBar );
		
		// housePanel
		housePanel = new GUIDicePanel( );
		housePanel.setHouse();
		guiFrame.add( housePanel );
		
		// StatusBar
		statusBar = new GUIStatusBar( );
		guiFrame.add( statusBar );
				
		//---------------------------------------------------------------------
		
	    // Display the window.
//  	frame.pack();
		guiFrame.setVisible(true);
		
//		loadAllPlayers();
	}
	
	/**
	 * Called from the AddPlayerBox->AddPlayerButton
	 * Calls all relevant functions needed to create a player object
	 * @param name  Name of the new player
	 * @param initialPoints  Starting balance of the new player
	 */
	public void createPlayer( String name, int initialPoints )
	{
		++noOfPlayersLoaded;
		String playerID = String.valueOf( noOfPlayersLoaded );
		
		// Using currentPlayer variable as temporary storage, as well as 
		// setting it. The result is when a player is created, they are 
		// automatically selected
		currentPlayer = new SimplePlayer( playerID, name, initialPoints );
		gameEngine.addPlayer( currentPlayer );
		guiMenuBar.createPlayer( name, noOfPlayersLoaded );
	}
	
	/**
	 * Called from the PlayerSelectMenuListener, after a new player is selected
	 * from the pull down player menu
	 * @param playerID
	 */
	public void selectCurrentPlayer( String playerID )
	{
		// NOTE selectedPlayer is a string, to match the original source code
		selectedPlayer = playerID;
		currentPlayer = gameEngine.getPlayer(playerID);
		
		// Update playerPanel
		playerPanel.changePlayer( currentPlayer );
		
		// Update resultsBar
//		resultsBar.setText( "Please Place a Bet" , false );
		
		// Update statusBar
		statusBar.updateStatusBar( currentPlayer );
		
		checkBetButton();
	}
		
	/**
	 * Takes updates from GameEngineCallbackGUI and updates the PlayerPanel
	 * @param dicePair  Players roll
	 */
	public void updatePlayeRoll( DicePair dicePair )
	{
		playerPanel.setResults( dicePair.getDice1(), dicePair.getDice2() );
	}
	
	public void updateHouseRoll( DicePair dicePair )
	{
		housePanel.setResults( dicePair.getDice1(), dicePair.getDice2() );
	}
	
	/**
	 * Called after the house has rolled, and player scores have had their
	 * Wins or Losses updated. The GUI also needs to be updated
	 */
	public void updatePoints()
	{
		statusBar.updateStatusBar( currentPlayer );
	}
	
	// Enable / Disable the place bet if the player has / hasn't already placed a bet
	private void checkBetButton( )
	{
		if ( currentPlayer.getBet() > 0 )
		{
			placeBetBox.enableBetButton(false);
		}
		else 
			placeBetBox.enableBetButton(true);
	}
	
	public void placeBet( int bet )
	{
		gameEngine.placeBet( currentPlayer, bet );
		System.out.println("Bet Placed");
		
		// Update the ResultsBar
//TODO		gui.updateResultsBar("Waiting for other players to bet", false);
		
		// Update the statusBar ( MAINLY Players current points )
		statusBar.updateStatusBar( currentPlayer );
		
		// Roll Player
		gameEngine.rollPlayer( currentPlayer,
				INITIALDELAY, FINALDELAY, DELAYINCREMENT);
		
		// Roll House ONLY if all players have rolled
		checkIfHouseCanRoll();
	}
	
	// Roll the house function ONLY if all players have bet
	private void checkIfHouseCanRoll()
	{		
		for ( Player data : gameEngine.getAllPlayers() )
		{
			if ( data.getBet() == 0 )
			{
				return;
			}
		}
		gameEngine.rollHouse(INITIALDELAY, FINALDELAY, DELAYINCREMENT);
		
		resultsBar.showEndRoundBtn( true );
	}	
	
	/**
	 * Function to clear all relevant variables to still store data from the
	 * last game, ready for the new game
	 */
	public void clearLastGame()
	{
		// Clear each of the player variables
		// Reset the players currentBet, ready for the next round NOTE that
		// even though we are using the method "placeBet", this will ONLY set 
		// the PRIVATE variable currentBet to 0. NO OTHER actions will be taken
		// NOTE this method is used in place of a setter, in an attempt to keep
		// currentBet PRIVATE
		for ( Player player : gameEngine.getAllPlayers() )
		{
			player.placeBet( 0 );
			player.setRollResult( null );
		}
		
		placeBetBox.enableBetButton( true );
		
		// Update playerPanel
		playerPanel.setResults( 0, 0 );
		
		resultsBar.showEndRoundBtn( false );
		
		// Clear house variables
		housePanel.setResults( 0, 0 );
	}
}