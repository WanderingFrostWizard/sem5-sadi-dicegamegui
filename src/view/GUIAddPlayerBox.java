package view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import controller.AddPlayerListener;

@SuppressWarnings("serial")
public class GUIAddPlayerBox extends JPanel
{
	private JTextField nameField;
	private JTextField pointsField;
	
	public String getNameFieldText()
	{
		return nameField.getText();
	}
	public String getPointsFieldText()
	{
		return pointsField.getText();
	}
	
	public GUIAddPlayerBox( GUIWindow gui )
	{
		this.setLayout( new GridLayout() );
		
		Dimension textSize = new Dimension( 100, 30 );
		
		JLabel nameTxt = new JLabel("PlayerName");
		nameTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		
		nameField = new JTextField();
		nameField.setMinimumSize(textSize);
		nameField.setHorizontalAlignment( JTextField.CENTER );
		nameField.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		JLabel pointsTxt = new JLabel("Initial Points");
		pointsTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		
		pointsField = new JTextField();
		pointsField.setMinimumSize(textSize);
		pointsField.setHorizontalAlignment( JTextField.CENTER );
		pointsField.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		JButton addPlayerBtn = new JButton("Add Player");
		addPlayerBtn.setBorder(new BevelBorder(BevelBorder.RAISED));
	
		/*
		 *  Add an ActionListener to allow this button to receive clicks
		 *  NOTE this functionality WILL NOT be used anywhere else
		 *  HENCE why normally I would create this method LOCALLY to avoid 
		 *  ANY possibility of outside access, as well as making variable
		 *  access from within the listener easier
		 */
		addPlayerBtn.addActionListener( new AddPlayerListener( gui, this ) );
		
		add(nameTxt);
		add(nameField);
		add(pointsTxt);
		add(pointsField);
		add(addPlayerBtn);
	}
	
	public void resetTxt()
	{
		nameField.setText( "" );
		pointsField.setText( "" );
	}
}
