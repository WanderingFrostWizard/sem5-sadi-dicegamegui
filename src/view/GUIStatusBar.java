package view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import model.interfaces.Player;

@SuppressWarnings("serial")
public class GUIStatusBar extends JPanel
{
	private JLabel playerNameTxt;
	private JLabel playerBetTxt;
	private JLabel playerPointsTxt;
	
	public GUIStatusBar()
	{
		final int NOOFCOMPONENTS = 6;
		this.setLayout(new GridLayout() );
		
		int totalWidth = GUIWindow.WIDTH - ( NOOFCOMPONENTS * 20 );		
		Dimension txtDimension = new Dimension(totalWidth / NOOFCOMPONENTS, 25);
		
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		// Add the statusPanel to the current JPanel
		add(statusPanel);
		
		// Descriptive label to describe the integer displayed in the next label
		JLabel playerNameHelperTxt = new JLabel("Current Player ");
		playerNameHelperTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		playerNameHelperTxt.setPreferredSize(txtDimension);
		
		// Label to display the players ID and name eg     2 - PLAYERNAME
		playerNameTxt = new JLabel();
		playerNameTxt.setHorizontalAlignment(SwingConstants.CENTER);
		playerNameTxt.setPreferredSize(txtDimension);
		playerNameTxt.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		// Descriptive label to describe the integer displayed in the next label
		JLabel playerBetHelperTxt = new JLabel("Current Player Bet ");
		playerBetHelperTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		playerBetHelperTxt.setPreferredSize(txtDimension);
		
		// Label to display the players ID and name eg     2 - PLAYERNAME
		playerBetTxt = new JLabel();
		playerBetTxt.setHorizontalAlignment(SwingConstants.CENTER);
		playerBetTxt.setPreferredSize(txtDimension);
		playerBetTxt.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		// Descriptive label to describe the integer displayed in the next label
		JLabel playerPointsHelperTxt = new JLabel("Current Points ");
		playerPointsHelperTxt.setHorizontalAlignment(SwingConstants.RIGHT);
		playerPointsHelperTxt.setPreferredSize(txtDimension);
		
		playerPointsTxt = new JLabel();
		playerPointsTxt.setHorizontalAlignment(SwingConstants.CENTER);
		playerPointsTxt.setPreferredSize(txtDimension);
		playerPointsTxt.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		statusPanel.add(playerNameHelperTxt);
		statusPanel.add(playerNameTxt);
		statusPanel.add(playerBetHelperTxt);
		statusPanel.add(playerBetTxt);
		statusPanel.add(playerPointsHelperTxt);
		statusPanel.add(playerPointsTxt);
	}

	protected void updateStatusBar( Player player )
	{
		playerNameTxt.setText( player.getPlayerId() + " - " + player.getPlayerName() );
		playerBetTxt.setText( String.valueOf( player.getBet() ) );
		playerPointsTxt.setText( String.valueOf( player.getPoints() ) );
	}
}
