package view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import controller.PlayerSelectMenuListener;

@SuppressWarnings("serial")
public class GUIMenuBar extends JPanel
{
	private ArrayList <JMenuItem> menuItems = new ArrayList<JMenuItem>();
	private JMenuBar menuBarObj;
	private JMenu playerMenu;
	private PlayerSelectMenuListener menuListener;
	// Store a reference to the GUIWindow object, to call methods on it
	
	public JMenuBar getMenuBarObj()
	{
		return this.menuBarObj;
	}
	
	public GUIMenuBar( GUIWindow gui )
	{
		menuListener = new PlayerSelectMenuListener( gui );
		
		// Create Menu Bar
		menuBarObj = new JMenuBar();
		menuBarObj.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		// Create player pull-down Menu to add to Menu Bar
		playerMenu = new JMenu("Player");
		playerMenu.setMnemonic(KeyEvent.VK_P);

		menuBarObj.add(playerMenu);
	}
	
	/**
	 * Called from GUIWindow, directly after a call from the AddPlayerListener
	 * This function creates a menuItem for the player, and adds them to the
	 * gameEngine
	 * 
	 * @param name  PlayerName
	 * @param playerID   PlayerID as an int (Converted within this function)
	 */
	//TODO  CANT Add the shortcut key ALT-PLAYER no, without a way to loop this in creation
	protected void createPlayer( String name, int playerID )
	{
		JMenuItem newItem = new JMenuItem( playerID + "-" + name, null );
//		newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_DOWN_MASK ));
		
		newItem.setActionCommand( String.valueOf( playerID ));
		newItem.addActionListener(menuListener);
		
		// Add to the menuItems ArrayList
		menuItems.add(newItem);
		
		playerMenu.add(newItem);
	}
}