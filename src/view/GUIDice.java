package view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Creates a grid like this, where the cell numbers represent the labels that
 * are used to display the values on the dice 
 * NOTE that only the numbered cells displayed are needed to represent digits
 *  -------------
 *  | 0 |   | 2 |
 *  -------------
 *  | 3 | 4 | 5 |
 *  -------------
 *  | 6 |   | 8 |
 *  -------------
 *  The function displayValue( int value ) is used to translate the value
 *  into a Graphical display on the grid
 */
@SuppressWarnings("serial")
public class GUIDice extends JPanel
{
	private final int NOOFCELLS = 9;
	final ImageIcon DOT = new ImageIcon("circle_blue.png");
//	final ImageIcon DOT = new ImageIcon("CIRCLE.png");
	
//TODO	private JLabel [] cellArray;
	private JButton [] cellArray;
	
	
	public GUIDice()
	{
		this.setLayout( new GridLayout(3, 3 ) );
		
//TODO		cellArray = new JLabel [NOOFCELLS];
		cellArray = new JButton [NOOFCELLS];
		
		// Initialize the array
		for ( int i = 0; i < NOOFCELLS; ++i )
		{
			cellArray[i] = new JButton();
			cellArray[i].setBackground(Color.WHITE);
			
//TODO Use this when I have finished setting up the dice part of the screen
//			cellArray[i] = new JLabel();
//			cellArray[i].setPreferredSize(txtDimension);
			
			// Add the cell to the JPanel
			add(cellArray[i]);
		}
	}
	
	/**
	 * To display the dice values, I have decided to be a little sneaky
	 * before a new value is displayed, the dice is "cleared", then the 
	 * new value is determined. Some numbers eg THREE, can be drawn by using
	 * the function for ONE and TWO, to set the correct icons to display 
	 * a THREE
	 * @param value
	 */
	public void displayValue( int value )
	{
		clearDice();
		
		switch(value)
		{
		case 1:
			displayONE();
			break;
		case 2:
			displayTWO();
			break;
		case 3:
			displayONE();
			displayTWO();
			break;
		case 4:
			displayTWO();
			displayFOUR();
			break;
		case 5:
			displayONE();
			displayTWO();
			displayFOUR();
			break;
		case 6:
			displayTWO();
			displayFOUR();
			displaySIX();
			break;
		}
	}
	
	private void clearDice()
	{
		for ( int i = 0; i < NOOFCELLS; ++i )
		{
			cellArray[i].setIcon(null);
		}
	}
	
	/*  -------------
	 *  |   |   |   |
	 *  -------------
	 *  |   | 4 |   |
	 *  -------------
	 *  |   |   |   |
	 *  -------------
	 */
	private void displayONE()
	{
		cellArray[4].setIcon(DOT);
	}
	
	/*  -------------
	 *  | 0 |   |   |
	 *  -------------
	 *  |   |   |   |
	 *  -------------
	 *  |   |   | 8 |
	 *  -------------
	 */
	private void displayTWO()
	{	
		cellArray[0].setIcon(DOT);
		cellArray[8].setIcon(DOT);
	}
	
	/*  -------------
	 *  | 0 |   | 2 |
	 *  -------------
	 *  |   |   |   |
	 *  -------------
	 *  | 6 |   | 8 |
	 *  -------------
	 *  0, 8 from displayTWO()
	 */
	private void displayFOUR()
	{	
		cellArray[2].setIcon(DOT);
		cellArray[6].setIcon(DOT);
	}
	
	/*  -------------
	 *  | 0 |   | 2 |
	 *  -------------
	 *  | 3 |   | 5 |
	 *  -------------
	 *  | 6 |   | 8 |
	 *  -------------
	 *  0, 8 from displayTWO()
	 *  2, 6 from displayFOUR()
	 */
	private void displaySIX()
	{
		cellArray[3].setIcon(DOT);
		cellArray[5].setIcon(DOT);
	}
}
