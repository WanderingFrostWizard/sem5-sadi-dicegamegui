package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import controller.EndRoundBtnListener;

@SuppressWarnings("serial")
public class GUIResultsBar extends JPanel
{
	private JLabel resultsTxt;
	private JButton endRoundBtn;
	
	public GUIResultsBar( GUIWindow gui )
	{
		this.setLayout( new GridLayout() );
				
		resultsTxt = new JLabel( "Please Select a Player" );
		resultsTxt.setHorizontalAlignment(SwingConstants.CENTER);
		resultsTxt.setPreferredSize( new Dimension( GUIWindow.WIDTH - 40, 20 ) );
		
		endRoundBtn = new JButton( "Start Next Round" );
		endRoundBtn.setBackground( Color.YELLOW );
		endRoundBtn.addActionListener( new EndRoundBtnListener( gui ) );
		endRoundBtn.setVisible( false );
		
		add(resultsTxt);
		add(endRoundBtn);
	}
	
	public void setText( String text, boolean win )
	{
		if ( win )
		{
			//TODO Set borders to be green or something
		}
		else 
		{
			//TODO RESET borders
		}
		
		resultsTxt.setText( text );
	}
	
	// Function to look at the game state and set the current text for the
	// current player
	public void updateState()
	{

//		// RESULTS BAR UPDATES
//			
//		// If player has yet to bet
//		resultsBar.setText( "Please Place a Bet" , false );
//			
//		// If a player has placed a bet
//		resultsBar.setText( "Waiting for other players to bet" , false );
//			
//		// If a player has won
//		resultsBar.setText( "You win ", true );
//			
//		// If a player has lost
//		resultsBar.setText( "Sorry, you lost", false );
	}
	
	public void showEndRoundBtn( boolean status )
	{
		endRoundBtn.setVisible( status );
	}
}
