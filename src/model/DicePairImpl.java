package model;

import model.interfaces.DicePair;

/**
 *  A pair of dice values. NOTE that these values are set when this class is
 *  created.
 * @author Cameron Watt s3589163
 *
 */
public class DicePairImpl implements DicePair {

	private int dice1;
	private int dice2;
	private int numFaces;
	
	public DicePairImpl(int dice1, int dice2, int numFaces)
	{
		this.dice1 = dice1;
		this.dice2 = dice2;
		this.numFaces = numFaces;
	}
	
	/**
	 * @return value of dice 1 (1 .. numFaces i.e. 1 .. 6 for standard casino 
	 * dice/die)
	 */
	public int getDice1()
	{
		return dice1;
	}

	/**
	 * @return value of dice 2 (1 .. numFaces i.e. 1 .. 6 for standard casino 
	 * dice/die)
	 */
	public int getDice2()
	{
		return dice2;
	}

	/**
	 * @return return number of faces (standard casino dice have 6) Dungeons 
	 *         and Dragons dice have more!
	 */
	public int getNumFaces()
	{
		return numFaces;
	}

	/**
	 * @return a human readable String that lists the values of this DicePair
	 *         instance (see OutputTrace.txt)
	 */
	@Override
	public String toString()
	{
		/*
		 *  Create a temporary String and concatenate the dice values to it
		 *  before returning it to the calling function
		 *  OUTPUT =   Dice 1 = # : Dice 2 = # : Total = ##
		 */
		String tempStr = "Dice 1 = ";
		tempStr += Integer.toString(dice1);
		tempStr += " : Dice 2 = ";
		tempStr += Integer.toString(dice2);
		tempStr += " : Total = ";
		tempStr += Integer.toString( dice1 + dice2 );
		
		return tempStr;
	}
}
