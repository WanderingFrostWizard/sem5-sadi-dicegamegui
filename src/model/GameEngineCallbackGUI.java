package model;

import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.GameEngineCallback;
import model.interfaces.Player;
import view.GUIWindow;

public class GameEngineCallbackGUI implements GameEngineCallback
{
	private GUIWindow gui;	
	
	public GameEngineCallbackGUI( GameEngine gameEngine )
	{
		gui = new GUIWindow( gameEngine );
		
		// Start a GUI game
	}
	
	@Override
	public void intermediateResult(Player player, DicePair dicePair, GameEngine gameEngine) 
	{
		// Check if the player placed a bet
		if ( player.getBet() == 0 )
		{
			//TODO  ??? WHAT NEEDS TO BE DONE HERE
			return;
		}
		
		// Update individual player gui to include these results ONLY if this
		// is the current player
		if ( player.getPlayerId().equals( gui.getSelectedPlayerID() ) )
		{
			// Update GUI
			gui.updatePlayeRoll( dicePair );
		}
		else
		{
			// ???
		}
	}

	@Override
	public void result(Player player, DicePair result, GameEngine gameEngine) 
	{
		// Update Player GUI
		gui.updatePlayeRoll( result );
	}

	@Override
	public void intermediateHouseResult(DicePair dicePair, GameEngine gameEngine) 
	{
		gui.updateHouseRoll( dicePair );
	}

	@Override
	public void houseResult(DicePair result, GameEngine gameEngine) 
	{
		// Update GUI with house Roll
		gui.updateHouseRoll( result );
		
		// Update the win/loss status for EACH PLAYER
		//TODO
		
		gui.updatePoints();		
	}

}
