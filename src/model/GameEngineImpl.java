package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.GameEngineCallback;
import model.interfaces.Player;		


/**
 * Assignment Implementation for SADI providing main Dice Game model 
 * functionality s1 2018
 * 
 * Uses default constructor
 * 
 * @author Cameron Watt
 */
public class GameEngineImpl implements GameEngine {

	// ArrayList to contain a record of each of the players
	private ArrayList <Player> playerList = new ArrayList <Player>();
	
	// Object to store the final roll of the House
	private DicePair houseRoll = null;
	
	// Reference to GameEngineCallback to allow for logging of data
	private ArrayList <GameEngineCallback> callbackList = new ArrayList <GameEngineCallback>();
	
	// Random Number generator to generate dice rolls
	Random randomNoGen = new Random();
	
	/**
	 * the implementation should forward the call to the player class so the bet is set per player
	 * @see Player#placeBet(int)
	 * 
	 * @param player
	 *            the player who is placing the bet
	 * @param bet
	 *            the bet in points
	 * @return true if the player had sufficient points and the bet was placed
	 */
	public boolean placeBet(Player player, int bet)
	{
		return player.placeBet(bet);	
	}

	private DicePair rollDice( )
	{
		int dice1, dice2;
		
		dice1 = 1 + randomNoGen.nextInt(10) % NUM_FACES;
		dice2 = 1 + randomNoGen.nextInt(10) % NUM_FACES;
		
		return ( new DicePairImpl( dice1, dice2, NUM_FACES ) );
	}
	
	/**
	 * roll the dice progressing from the initialDelay to the finalDelay in
	 * increments of delayIncrement, delays are in milliseconds (ms)
	 * 
	 * 1. start at initialDelay then increment the delay each time a new number
	 *    is shown on the die faces 
	 * 2. call GameEngineCallback.intermediateResult(...) or intermediateHouseResult(...) each time 
	 *    a pair of new dice faces are shown until delay greater than or equal to finalDelay 
	 * 3. call GameEngineCallback.result(...) or houseResult(...) with final result for player or house 
	 * 4. make sure you update the player with final result so it can be retreived later
	 * 
	 * @param player
	 *            the player who is rolling and will have their result set
	 *            at the end of the roll
	 * @param initialDelay
	 *            the starting delay in ms between updates (based on how fast
	 *            dice are rolling)
	 * @param finalDelay
	 *            the final delay in ms between updates when the dice stop
	 *            rolling
	 * @param delayIncrement
	 *            how much the dice slow down (delay gets longer) after each
	 *            roll/tumble
	 * 
	 * @see model.interfaces.GameEngineCallback
	 * 
	 */
	public void rollPlayer(Player player, int initialDelay, int finalDelay, int delayIncrement) 
	{
		DicePair dicePair = null;
		int currDelay = initialDelay;
		
		while ( currDelay < finalDelay )
		{
			dicePair = rollDice();
			for ( int i = 0; i < callbackList.size(); ++i )
			{
				callbackList.get( i ).intermediateResult(player, dicePair, null);
			}
			
			/*
			 *  If player did not place a bet, it has already been logged above 
			 *  in intermediateResult, now need to exit function
			 */
			if ( player.getBet() == 0 )
			{
				player.setRollResult( new DicePairImpl( 0, 0, 6 ) );
				return;
			}
				
			
			// Increment currDelay each time a new number is shown on die faces
			currDelay += delayIncrement;
		}
		
		// RE-ROLL for FINAL RESULT
		dicePair = rollDice();
		
		// 3 - Call GameEngineCallback.result(...) with final roll
		for ( int i = 0; i < callbackList.size(); ++i )
		{
			callbackList.get( i ).result(player, dicePair, null);
		}
		
		// 4 - Update Player with final Dice roll
		player.setRollResult( dicePair );
	}

	/**
	 * Same as rollPlayer() but rolls for the house and calls the house versions
	 * of the callback methods on GameEngineCallback, no player parameter is required. 
	 * 
	 * All bets are settled at the end of this method i.e. this method goes through all players 
	 * and applies win or loss to update betting points 
	 * (this functionality should be implemented in a separate private method in GameEngineImpl)
	 * 
	 * @param initialDelay same as rollPlayer()
	 * @param finalDelay same as rollPlayer()
	 * @param delayIncrement same as rollPlayer()
	 * 
	 * @see GameEngine#rollPlayer(Player, int, int, int)
	 */
	public void rollHouse(int initialDelay, int finalDelay, int delayIncrement) 
	{
		DicePair dicePair = null;
		int currDelay = initialDelay;

		while ( currDelay < finalDelay )
		{
			dicePair = rollDice();
			for ( int i = 0; i < callbackList.size(); ++i )
			{
				callbackList.get( i ).intermediateHouseResult(dicePair, null);
			}
			
			// Increment currDelay each time a new number is shown on die faces
			currDelay += delayIncrement;
		}
		
		// RE-ROLL for FINAL RESULT and Add to houseRoll
		houseRoll = rollDice();
		
		// Settle the bets for each player, applying win/loss to player points
		for ( int i = 0; i < playerList.size(); ++i )
		{
			settleBet( playerList.get( i ) );
		}
		
		// 3 - Call GameEngineCallback.result(...) with final roll
		for ( int i = 0; i < callbackList.size(); ++i )
		{
			callbackList.get( i ).houseResult(dicePair, this );
		}
	}

	/**
	 * @param player
	 *            to add to game
	 */
	public void addPlayer(Player player) 
	{
			playerList.add( player );
	}

	/**
	 * Search through the playerList to find a player object with the same id 
	 * and return it to the calling function
	 * I am using a temporary Player object to avoid 2x calls to the 
	 * playerList.get() function
	 * @param id
	 *            id of player to retrieve
	 * @return corresponding Player instance or null if Player doesn't exist
	 */
	public Player getPlayer(String id) 
	{
		Player temp;
		
		for ( int i = 0; i < playerList.size(); ++i )
		{
			temp = playerList.get( i );
			
			if ( id.equals(temp.getPlayerId() ) )
//			if ( temp.getPlayerId() == id )  ORIGINAL CODE from Assignment 1
				return temp;
		}
		return null;
	}

	/**
	 * @param player
	 *            to remove from game
	 * @return true if the player existed
	 */
	public boolean removePlayer(Player player) 
	{
		return playerList.remove( player );
	}

	/**
	 * @param gameEngineCallback
	 *            a client specific implementation of GameEngineCallback used to
	 *            perform display updates/logging
	 * 
	 *            you will write a different implementation for console and GUI
	 *            implementations (Assignments part 1 and 2)
	 */
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) 
	{
		callbackList.add( gameEngineCallback );
	}

	/**
	 * @param gameEngineCallback
	 *            called when a player quits the game to remove no longer needed
	 *            UI updates
	 * @return true if the gameEngineCallback existed
	 */
	public boolean removeGameEngineCallback(GameEngineCallback gameEngineCallback) 
	{
		return callbackList.remove( gameEngineCallback );
	}

	/**
	 * @return an unmodifiable collection of all Players
	 * @see model.interfaces.Player
	 */
	public Collection<Player> getAllPlayers() 
	{
		return Collections.unmodifiableList(playerList);
	}
	
	/**
	 * Function to calculate if the player has won or lost, and adjust their
	 * points accordingly
	 * 
	 * @param player Player to settle win or loss
	 */
	private void settleBet( Player player )
	{
		DicePair playerRoll = player.getRollResult();
		int playerTotal = playerRoll.getDice1() + playerRoll.getDice2();
		int houseTotal = houseRoll.getDice1() + houseRoll.getDice2();
		
		int playerPoints = player.getPoints();
		int playerBet = player.getBet();
		
		// Player Wins
		if ( playerTotal > houseTotal )
		{ 
			player.setPoints( playerPoints + ( 2 * playerBet ) );
		}
		
		/*  No action needed for House wins, or Draw as bet has already been
		 *  deducted, in placeBet()   
		 */
	}
}