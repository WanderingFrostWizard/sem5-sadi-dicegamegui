package model;

import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.GameEngineCallback;
import model.interfaces.Player;

/**
 * 
 * Skeleton example implementation of GameEngineCallback showing Java logging behavior
 * 
 * @author Caspar Ryan
 * @see model.interfaces.GameEngineCallback
 * 
 */
public class GameEngineCallbackImpl implements GameEngineCallback
{
	private Logger logger = Logger.getLogger("assignment2");
	private ConsoleHandler handler = new ConsoleHandler();
	
	public GameEngineCallbackImpl()
	{
		// LOGGING CODE FROM    https://stackoverflow.com/questions/9199598/using-java-util-logging-to-log-on-the-console
		//      https://stackoverflow.com/questions/31576775/java-util-logging-logger-prints-the-message-twice-in-console
		
		// FINE shows rolling output, INFO only shows result
		logger.setLevel(Level.FINE);	
		handler.setFormatter(new SimpleFormatter());
		logger.addHandler( handler );
		handler.setLevel(Level.FINE);
		logger.setUseParentHandlers(false);
	}

	@Override
	public void intermediateResult(Player player, DicePair dicePair, GameEngine gameEngine)
	{
		// Check if the player placed a bet
		if ( player.getBet() == 0 )
		{
			logger.fine( player.getPlayerName() + ": Did NOT place a bet " 
					+ " \n" + "-------------------------------"
					+ "----------------------------------------" );
			return;
		}
		
		// intermediate results logged at Level.FINE
		logger.fine( player.getPlayerName() + ": ROLLING " + dicePair.toString() );
	}

	// Print the results for this player, as well as a separation break
	@Override
	public void result(Player player, DicePair result, GameEngine gameEngine)
	{
		// final results logged at Level.INFO
		logger.info( player.getPlayerName() + ": # RESULT # " 
				+ result.toString() + " \n" + "-------------------------------"
				+ "----------------------------------------" );
	}

	@Override
	public void intermediateHouseResult(DicePair dicePair, GameEngine gameEngine) 
	{
		// intermediate results logged at Level.FINE
		logger.fine( "HOUSE : ROLLING " + dicePair.toString() );
	}

	@Override
	public void houseResult(DicePair result, GameEngine gameEngine) 
	{
		List <Player> playerList = (List<Player>) gameEngine.getAllPlayers();
		
		// final results logged at Level.INFO
		logger.info( "HOUSE : # RESULT # " + result.toString() 
				+ " \n" + "-------------------------------"
				+ "----------------------------------------" );;
		
		// Print the final results for each player
		for ( int i = 0; i < playerList.size(); ++i )
		{
			logger.info( playerList.get( i ).toString() );
		}
	}
}
